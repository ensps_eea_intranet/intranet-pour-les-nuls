# Intranet pour les nuls

Ce depot contient le code _Tex_ de l'**Intranet pour les nuls**.

Pour le lire vous pouvez :
- aller sur le lien [COURS](https://gitlab.com/ensps_eea_intranet/intranet-pour-les-nuls/-/jobs/artifacts/main/raw/cours.pdf?job=deploy-local) et telechargé la version compiler la plus recente,
- clonner le depos et utiliser la commande ```make all``` pour compiler.

## Participation

Pour participer à la rédaction, il suffit d'avoir un compte *gitlab* et de faire une *merge request*.
Les merge request permetent que le code soit uniformisé avant la fusion.

## Convention de style

Chaque UE est representer par une partie, chaque cours par un chapitre.
Chaque cours doit etre ecrit dans un fichier differents, puis ils sont assemblé ensemble dans le .tex principale.

Chaque phrase doit etre sur une ligne séparer (pour que la fusion de git se passe mieux).
