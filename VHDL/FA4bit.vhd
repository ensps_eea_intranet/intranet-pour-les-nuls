library ieee;
use ieee.std_logic_1164.all;

ENTITY FA4bit IS
    PORT (
        x1 : IN STD_LOGIC_VECTOR(3 downto 0);
        x2 : IN STD_LOGIC_VECTOR(3 downto 0);
        s : OUT STD_LOGIC_VECTOR(4 downto 0);
END FA4bit;

ARCHITECTURE basique of FA4bit IS
COMPONENT FA1bit IS
    PORT (
        x1 : IN STD_LOGIC;
        x2 : IN STD_LOGIC;
        ci : IN STD_LOGIC;
        s : OUT STD_LOGIC;
        co : OUT STD_LOGIC);
END COMPONENT;
signal c : STD_LOGIC_VECTOR(3 downto 0);
BEGIN
    add1 : FA1bit port map (x1(0), x2(0), '0', s(0), c(0));
    add2 : FA1bit port map (x1(1), x2(1), c(0), s(1), c(1));
    add3 : FA1bit port map (x1(2), x2(2), c(1), s(2), c(2));
    add4 : FA1bit port map (x1(3), x2(3), c(2), s(3), c(3));
    s(4) <= c(3);
END basique;