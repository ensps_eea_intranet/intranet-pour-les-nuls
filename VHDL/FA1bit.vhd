library ieee;
use ieee.std_logic_1164.all;

ENTITY FA1bit IS
    PORT (
        x1 : IN STD_LOGIC;
        x2 : IN STD_LOGIC;
        ci : IN STD_LOGIC;
        s : OUT STD_LOGIC;
        co : OUT STD_LOGIC);
END FA1bit;

ARCHITECTURE data_flow of FA1bit IS
BEGIN
    s <= x1 XOR x2 XOR ci;
    co <= (x1 AND x2) OR (x1 AND ci) OR (x2 AND ci);
END data_flow;