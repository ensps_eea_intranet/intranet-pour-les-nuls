
all : cours.pdf

syntax-check : cours.tex $(wildcard */*.tex)
	lualatex --shell-escape --draftmode --halt-on-error cours.tex | grep warning
	
cours.pdf : cours.tex $(wildcard */*.tex)
	lualatex --shell-escape cours.tex
	lualatex --shell-escape cours.tex

clean :
	rm -f cours.aux
	rm -f cours.log
	rm -f cours.out
	rm -f cours.pdf
	rm -f cours.toc
	rm -r -f _minted*