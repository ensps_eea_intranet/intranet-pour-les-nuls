FROM debian:11

RUN apt update \
&& apt install -yq make \
&& apt install -yq texlive-full \
&& apt install -yq git

RUN apt install -yq pip \
&& pip install pigments
