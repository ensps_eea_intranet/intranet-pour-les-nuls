import numpy as np

N = np.array(range(20))

R = [0.2, 0.1, 0.05, 0.01]

C = []

for r in R:
    C.append(np.sinc(N*r))

import matplotlib.pyplot as plt
fig, ax = plt.subplots(1,1)
for n, (r, cn) in enumerate(zip(R, C)):
    ax.stem(N, cn, label = "$\\frac{\\tau}{T_e} = " + str(r) + "$", linefmt="k:", markerfmt="C"+str(n)+"o", basefmt="k-")
ax.legend()
fig.tight_layout()
fig.savefig("TraitementDuSignal/fig_echantionneur_real_cn.eps")